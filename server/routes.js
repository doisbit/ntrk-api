const _ = require( 'lodash' );

module.exports = ( server ) => {
    return new Promise( async ( resolve, reject ) => {
        try{
            await require( '../api/driver' )( server );
            await require( '../api/client' )( server );
            await require( '../api/delivery' )( server );
            await require( '../api/location' )( server );
            resolve( server );
        }
        catch( ERR ){
            reject( ERR );
        }
    } );
};