const _ = require( 'lodash' );

module.exports = ( server ) => {
    return new Promise( ( resolve, reject ) => {
        // Load the client/driver user in req.user if needed so
        server.use(
            async ( req, res, next ) => {
                let associated;
                try{
                    if( req.user && [ 'client', 'driver' ].indexOf( req.user.roleKey ) > -1 ){
                        associated = await req.user[ `get${_.capitalize( req.user.roleKey )}` ]();
                        if( _.isNil( associated ) ) throw new Error( 'N_F' );
                        req.user.dataValues[ req.user.roleKey ] = associated;
                        return next();
                    }
                    else{
                        return next();
                    }
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        resolve( server );
    } );
};