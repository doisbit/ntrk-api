module.exports = ( db, type ) => {
    return new Promise( ( resolve, reject ) => {
        switch( type ){
        case( 'mysql' ):
            
            db.DEFINITIONS.Driver = db.import( '../models/mysql/driver' );
            db.DEFINITIONS.Client = db.import( '../models/mysql/client' );
            db.DEFINITIONS.Delivery = db.import( '../models/mysql/delivery' );
            db.DEFINITIONS.Customer = db.import( '../models/mysql/customer' );
            
            db.DEFINITIONS.Driver.hasMany( db.DEFINITIONS.Delivery, {
                as: 'deliveries',
                foreignKey: 'driverId',
            } );

            db.DEFINITIONS.Client.hasMany( db.DEFINITIONS.Delivery, {
                as: 'deliveries',
                foreignKey: 'clientId',
            } );
            
            db.DEFINITIONS.Customer.hasMany( db.DEFINITIONS.Delivery, {
                as: 'deliveries',
                foreignKey: 'customerId',
            } );

            db.DEFINITIONS.Driver.belongsTo( db.DEFINITIONS.User, {
                as: 'user',
                foreignKey: 'userId',
            } );
            
            db.DEFINITIONS.Client.belongsTo( db.DEFINITIONS.User, {
                as: 'user',
                foreignKey: 'userId',
            } );
            
            db.DEFINITIONS.Customer.belongsTo( db.DEFINITIONS.User, {
                as: 'user',
                foreignKey: 'userId',
            } );
            
            db.DEFINITIONS.Delivery.belongsTo( db.DEFINITIONS.Customer, {
                as: 'customer',
                foreignKey: 'customerId',
            } );

            db.DEFINITIONS.Delivery.belongsTo( db.DEFINITIONS.Driver, {
                as: 'driver',
                foreignKey: 'driverId',
            } );
            
            db.DEFINITIONS.Delivery.belongsTo( db.DEFINITIONS.Client, {
                as: 'client',
                foreignKey: 'clientId',
            } );

            db.DEFINITIONS.User.hasOne( db.DEFINITIONS.Client );
            db.DEFINITIONS.User.hasOne( db.DEFINITIONS.Driver );
            db.DEFINITIONS.User.hasOne( db.DEFINITIONS.Customer );

            resolve( db );

            break;
        default:
            resolve();
            break;
        }
    } );
};