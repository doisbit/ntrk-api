const unirest = require( 'unirest' );
const Config = require( 'config' );
const debug = require( 'debug' )( 'API:NOWTRACK:UTILS' );
const _ = require( 'lodash' );

/**
 * Utils class
 * 
 * @author Alexandre Moraes | alcmoraes89@gmail.com
 */
class Utils {
    /**
     * 
     * This routine calculates the distance between two points
     * in Kilometers (given the latitude/longitude of those points).
     * 
     * @param {String} lat1 first latitude to calculate
     * @param {String} lon1  first longitude to calculate
     * @param {String} lat2  second latitude to calculate
     * @param {String} lon2  first longitude to calculate
     * @return {String} The distance
     */
    distanceBetweenCoordinates( lat1, lon1, lat2, lon2 ){
        let radlat1 = Math.PI * lat1 / 180;
        let radlat2 = Math.PI * lat2 / 180;
        let theta = lon1 - lon2;
        let radtheta = Math.PI * theta / 180;
        let dist = Math.sin( radlat1 ) * Math.sin( radlat2 ) + Math.cos( radlat1 ) * Math.cos( radlat2 ) * Math.cos( radtheta );
        dist = Math.acos( dist );
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return dist;
    }


    /**
     * Check if a given coordinate is valid
     * 
     * @param {String} coordinate 
     * @return {Boolean}
     */
    isValidCoordinate( coordinate ){
        let re = /^-?([1-8]?[1-9]|[1 - 9]0)\.{1}\d{1,6}/;
        return re.test( coordinate );
    }

    /**
     * Returns a coordinate object from a given
     * address
     * 
     * @param {Object} body
     * @return {Promise} 
     */
    getCoordinatesFromAddress( body ){
        return new Promise( async ( resolve, reject ) => {

            let address = [
                'https://maps.googleapis.com/maps/api/geocode/json?',
                'key=',
                Config.get( 'google.maps.api' ),
                '&address=',
                body.address ? encodeURIComponent( body.address ) + ',' : '',
                body.address_2 ? encodeURIComponent( body.address_2 ) + ',' : '',
                body.city ? encodeURIComponent( body.city ) + ',' : '',
                body.state ? encodeURIComponent( body.state ) + ' - ' : '',
                body.zipcode ? encodeURIComponent( body.zipcode ) + ',' : '',
            ].join( '' );

            debug( address );

            unirest.get( address ).headers(
                { 'Accept': 'application/json' }
            ).end(
                ( response ) => {
                    response = response.body;
                    if( response.status !== 'OK' ){
                        debug( 'PROBLEM ON GOOGLE API REQUEST!' );
                        return resolve( { lat: null, lng: null } );
                    }
                    
                    if( _.has( response.results[ 0 ], 'geometry.location' ) ){
                        return resolve( response.results[ 0 ].geometry.location );
                    }

                    return resolve( { lat: null, lng: null } );
                }
            );
        } );
    }
}

module.exports = new Utils();