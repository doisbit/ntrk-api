const Utils = require( '../../../../services/utils' );

/**
* Definition for Client model
*
* @param  {Sequelize\ORM} db Sequelize database object
* @param  {Sequelize\DataTypes} DataTypes Sequelize DataTypes helper
* @return {Sequelize}    Sequelize database object
*/
module.exports = ( db, DataTypes ) => {
    return db.define( 'client', {
        id: {
            type: DataTypes.CHAR( 36 ),
            primaryKey: true,
            allowNull: false,
            defaultValue: ( new Utils().generateUuid ),
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        document: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        logo: {
            type: DataTypes.STRING,
            defaultValue: 'https://ntrk.co/assets/img/default_client_logo.png',
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        address_2: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        zipcode: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        state: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
        },
        userId: {
            type: DataTypes.CHAR(36),
            isUnique: true,
        },
    }, {
        paranoid: true,
    } );
};