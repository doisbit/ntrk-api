const Utils = require( '../../../../services/utils' );

/**
* Definition for Driver model
*
* @param  {Sequelize\ORM} db Sequelize database object
* @param  {Sequelize\DataTypes} DataTypes Sequelize DataTypes helper
* @return {Sequelize}    Sequelize database object
*/
module.exports = ( db, DataTypes ) => {
    return db.define( 'driver', {
        id: {
            type: DataTypes.CHAR( 6 ),
            primaryKey: true,
            allowNull: false,
            defaultValue: ( new Utils().generateKey ),
        },
        device_type: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        device_version: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
        },
        userId: {
            type: DataTypes.CHAR(36),
            isUnique: true,
        },
        speed: {
            type:      DataTypes.DOUBLE,
            allowNull: true,
            validate:  {
                isDecimal: true,
            },
        },
        lat: {
            type:      DataTypes.DOUBLE,
            allowNull: true,
            validate:  {
                isDecimal: true,
            },
        },
        lng: {
            type:      DataTypes.DOUBLE,
            allowNull: true,
            validate:  {
                isDecimal: true,
            },
        },
    }, {
        paranoid: true,
    } );
};