const Utils = require( '../../../../services/utils' );

/**
* Definition for Delivery model
*
* @param  {Sequelize\ORM} db Sequelize database object
* @param  {Sequelize\DataTypes} DataTypes Sequelize DataTypes helper
* @return {Sequelize}    Sequelize database object
*/
module.exports = ( db, DataTypes ) => {
    return db.define( 'customer', {
        id: {
            type: DataTypes.CHAR(6),
            primaryKey: true,
            allowNull: false,
            defaultValue: ( new Utils().generateKey ),
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lat: {
            type:      DataTypes.DOUBLE,
            allowNull: true,
            validate:  {
                isDecimal: true,
            },
        },
        lng: {
            type:      DataTypes.DOUBLE,
            allowNull: true,
            validate:  {
                isDecimal: true,
            },
        },
        address_2: DataTypes.STRING,
        zipcode: DataTypes.STRING,
        city: DataTypes.STRING,
        state: DataTypes.STRING,
    }, {
        paranoid: true,
    } );
};