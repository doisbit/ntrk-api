const Utils = require( '../../../../services/utils' );

/**
* Definition for Delivery model
*
* @param  {Sequelize\ORM} db Sequelize database object
* @param  {Sequelize\DataTypes} DataTypes Sequelize DataTypes helper
* @return {Sequelize}    Sequelize database object
*/
module.exports = ( db, DataTypes ) => {
    return db.define( 'delivery', {
        id: {
            type: DataTypes.CHAR(6),
            primaryKey: true,
            allowNull: false,
            defaultValue: ( new Utils().generateKey ),
        },
        client_observation: DataTypes.STRING,
        customer_observation: DataTypes.STRING,
        status: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
        },
    }, {
        paranoid: true,
    } );
};