const Joi = require( 'joi' );
const config = require( 'config' );
const debug = require( 'debug' )( 'API:PLUGINS:NOWTRACK:ROUTES:CLIENT' );
const _   = require( 'lodash' );

const schema = Joi.object().keys( {
    name: Joi.string().optional(),
    document: Joi.string().optional(),
    logo: Joi.string().optional(),
    address: Joi.string().optional(),
    address_2: Joi.string().optional(),
    phone: Joi.string().optional(),
    zipcode: Joi.string().optional(),
    city: Joi.string().optional(),
    state: Joi.string().optional(),
    status: Joi.string().optional(),
    userId: Joi.string().optional(),
    location: Joi.string().optional(),
} );

/**
 * @swagger
 * resourcePath: /clients
 * description: Clients endpoint
 */
module.exports = ( server ) => {
    return new Promise( ( resolve, reject ) => {
        /**
         * @swagger
         * path: /clients
         * operations:
         *   -  httpMethod: POST
         *      summary: Create client
         *      notes: Return the created client
         *      responseClass: String
         *      nickname: new_client
         *      consumes:
         *        - text/html
         *      parameters:
        */
        server
            .post( {
                name: 'new_client',
                path: `${config.get( 'router.prefix' )}/clients`,
                version: '1.0.0',
            },
            server.UTILS.aclMiddleware( [ 'root', 'admin', 'user' ] ),
            async ( req, res, next ) => {
                let client;
                let created;
                try{
                    await Joi.validate( req.body, schema );
                    
                    req.body.userId = req.user.id;
                    req.body.status = 1;

                    [ client, created ] = await server.DB_MYSQL.models.client.findOrCreate(
                        { where: { userId: req.user.id }, defaults: req.body }
                    );

                    if( _.isNil( client ) ) throw new Error( 'SV_RTRN_NIL' );
                    if( !created ) throw new Error( 'ALRD_CRTD' );

                    // Change the roleKey to client
                    await req.user.update( { roleKey: 'client' } );

                    return server.UTILS.jsonResponse( req, res, _.omit( client.get( { plain:true } ) ) );
                }
                catch( ERR ){
                    debug( ERR.stack );
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        /**
         * @swagger
         * path: /clients
         * operations:
         *   -  httpMethod: GET
         *      summary: Get clients
         *      notes: Return the clients
         *      responseClass: String
         *      nickname: get_clients
         *      consumes:
         *        - text/html
         */
        server
            .get( {
                name: 'get_clients',
                path: `${config.get( 'router.prefix' )}/clients`,
                version: '1.0.0',
            },
            server.UTILS.aclMiddleware( [ 'root', 'admin', 'client' ] ),
            async ( req, res, next ) => {
                let clients;
                let conditions;
            
                conditions = {
                    where: {},
                };

                if( req.query.q ){
                    conditions.where.name = {
                        '$like': `%${req.query.q}`,
                    };
                }

                if( req.user.roleKey == 'client' ){
                    conditions.where.userId = req.user.id;
                }

                try{
                    clients = await server.DB_MYSQL.models.client.findAll( conditions );
                    return server.UTILS.jsonResponse( req, res, clients );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        /**
         * If the path matches clients/:id set the entity into the request object
         */
        server.use(
            async ( req, res, next ) => {
                switch( true ){
                case( new RegExp( 'clients/:id' ).test( req.route.path ) ):
                    let client;
                    try{
                        client =
                            await server.DB_MYSQL.models.client.findOne( {
                                where: {
                                    id: req.params.id,
                                },
                                include: [
                                    {
                                        model: server.DB_MYSQL.models.user,
                                        as: 'user',
                                    },
                                    {
                                        model: server.DB_MYSQL.models.delivery,
                                        as: 'deliveries',
                                    },
                                ],
                                raw: false,
                            } );
                        if( _.isNil( client ) ) throw new Error( 'N_F' );
                        req.entity = client;
                        next();
                    }
                    catch( ERR ){
                        await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                        return server.UTILS.sendError( req, res, ERR );
                    }
                    break;
                default:
                    next();
                }
            }
        );
        
        /**
         * @swagger
         * path: /clients/{id}
         * operations:
         *   -  httpMethod: GET
         *      summary: Get an client
         *      notes: Return the client
         *      responseClass: String
         *      nickname: get_client
         *      consumes:
         *        - text/html
         *      parameters:
         *        - name: id
         *          in: path
         *          description: Client ID
         *          paramType: String
         *          required: true
         */
        server
            .get( {
                name: 'get_client',
                path: `${config.get( 'router.prefix' )}/clients/:id`,
                version: '1.0.0',
            },
            server.UTILS.ownershipMiddleware( 'user.id', 'entity.userId' ),
            async ( req, res, next ) => {
                try{
                    return server.UTILS.jsonResponse( req, res, req.entity );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );
        
        /**
        * @swagger
        * path: /clients/{id}
        * operations:
        *   -  httpMethod: PUT
        *      summary: Update an client
        *      notes: Return the updated client
        *      responseClass: String
        *      nickname: edit_client
        *      consumes:
        *        - text/html
        *      parameters:
        *        - name: id
        *          in: path
        *          description: Client ID
        *          paramType: String
        *          required: true
        *        - name: name
        *          description: The client name
        *          paramType: form
        *          required: false
        *          dataType: String
        *        - name: email
        *          description: The client email
        *          paramType: form
        *          required: false
        *          dataType: String
        *        - name: password
        *          description: The client password
        *          paramType: form
        *          required: false
        *          dataType: String
        */
        server
            .put( {
                name: 'edit_client',
                path: `${config.get( 'router.prefix' )}/clients/:id`,
                version: '1.0.0',
            },
            server.UTILS.ownershipMiddleware( 'user.id', 'entity.userId' ),
            async ( req, res, next ) => {
                let client;
                try{
                    await Joi.validate( req.body, schema );

                    if( !_.isNil( req.body.password ) ){
                        req.body.salt = bcrypt.genSaltSync( 8 );
                        req.body.password = bcrypt.hashSync( req.body.password, req.body.salt );
                    }

                    client = await req.entity.update( req.body );
                    if( _.isNil( client ) ) throw new Error( 'SV_RTRN_NIL' );
                    return server.UTILS.jsonResponse( req, res, _.merge( client, req.body ) );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );
        
        /**
         * @swagger
         * path: /clients/{id}
         * operations:
         *   -  httpMethod: DELETE
         *      summary: Delete an client by ID
         *      notes: Deletes an client from a given ID
         *      responseClass: String
         *      nickname: delete_client
         *      consumes:
         *        - text/html
         *      parameters:
         *        - name: id
         *          in: path
         *          description: Client ID
         *          paramType: String
         *          required: true
        */
        server
            .del( {
                name: 'delete_client',
                path: `${config.get( 'router.prefix' )}/clients/:id`,
                version: '1.0.0',
            },
            server.UTILS.ownershipMiddleware( 'user.id', 'entity.userId' ),
            async ( req, res, next ) => {
                try{
                    await req.entity.destroy();
                    return server.UTILS.jsonResponse( req, res );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );
        
        resolve( server );
    } );
};