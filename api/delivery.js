const Joi = require( 'joi' );
const debug = require( 'debug' )( 'API:NOWTRACK:DELIVERY' );
const config = require( 'config' );
const _   = require( 'lodash' );
const bcrypt = require( 'bcryptjs' );
const Utils = require( '../services/utils' );

const schema = Joi.object().keys( {
    id: Joi.string().optional(),
    address: Joi.string().optional(),
    latitude: Joi.string().optional(),
    longitude: Joi.string().optional(),
    observation: Joi.string().optional(),
    phone: Joi.string().optional(),
    status: Joi.number().optional(),
    name: Joi.string().optional(),
    driverId: Joi.string().optional(),
} );

/**
 * @swagger
 * resourcePath: /deliveries
 * description: Deliveries endpoint
 */
module.exports = ( server ) => {
    return new Promise( ( resolve, reject ) => {

        /**
         * Try to match a phone number if given information mismatch the stored one.
         * 
         * Will match the following patterns
         * 
         * StoredN = 5548991674935 | GivenN = 91674935
         * StoredN = 91674935 | GivenN = 5548991674935
         * @param {Number} givenN 
         * @param {Number} storedN 
         * @return {Boolean} If phone numbers matches
         */
        let ensurePhoneMatch = ( givenN, storedN ) => {
            if( _.isNil( givenN ) || _.isNil( storedN ) ) return false;
            // First let's strip any non character from both numbers
            givenN = givenN.toString().replace( /\D/g, '' );
            storedN = storedN.toString().replace( /\D/g, '' );
            // If user sent a different length number from the current stored.
            // He maybe sent without a Country Code or the City Code. Let's try fix it.
            if( givenN.length > storedN.length ) givenN = givenN.substring( givenN.length - storedN.length );
            if( givenN.length < storedN.length ) storedN = storedN.substring( storedN.length - givenN.length );
            return storedN == givenN || `9${storedN}` == givenN || storedN == `9${givenN}`;
        };

        /**
         * @swagger
         * path: /deliveries
         * operations:
         *   -  httpMethod: POST
         *      summary: Create delivery
         *      notes: Return the created delivery
         *      responseClass: String
         *      nickname: new_delivery
         *      consumes:
         *        - text/html
         *      parameters:
         */
        server
            .post( {
                name: 'new_delivery',
                path: `${config.get( 'router.prefix' )}/deliveries`,
                version: '1.0.0',
            },
            server.UTILS.aclMiddleware( [ 'root', 'admin', 'client' ] ),
            async ( req, res, next ) => {
                let client;
                let trySave;
                let delivery;
                let salt = bcrypt.genSaltSync( 8 );
                let password = bcrypt.hashSync( req.body.phone, salt );
                let customer_user;
                let customer_body;
                let customer_coordinates = { lat: null, lng: null };
                let customer;
                let driver;
                let created;
                req.body.phone = req.body.phone.replace( /[^0-9]/g, '' );
                try{
                    if( req.user.roleKey == 'client' ){
                        client = await req.user.getClient();
                        if( _.isNil( client ) ) throw new Error( 'N_F' );
                        req.body.clientId = client.id;
                    }

                    req.body.status = 1;
                    
                    trySave = async () => {
                        [ delivery, created ] = await server.DB_MYSQL.models.delivery.findOrCreate(
                            {
                                where: {
                                    id: server.UTILS.generateKey( false, 6 ),
                                    status: {
                                        $in: [ 1, 2, 3, 4 ],
                                    },
                                },
                                defaults: req.body,
                            }
                        );
                        if( _.isNil( delivery ) || !created ) return trySave();

                        await server.UTILS.sendPushNotification( server.DB_MYSQL, driver.userId, 'Bip bip!', 'Nova requisição de entrega!' );

                        return server.UTILS.jsonResponse( req, res, delivery.get( { plain: true } ) );
                    };
                    
                    driver = await server.DB_MYSQL.models.driver.findOne( { where: { id: req.body.driverId } } );

                    [ customer_user, created ] = await server.DB_MYSQL.models.user.findOrCreate(
                        {
                            where: {
                                email: `${req.body.phone}@nowtrack.com.br`,
                            },
                            defaults: {
                                name: req.body.name || req.body.phone,
                                password: password,
                            },
                        }
                    );

                    customer_body = req.body;
                    if( !_.has( req.body, 'lat' ) || !_.has( req.body, 'lng' ) ) customer_coordinates = await Utils.getCoordinatesFromAddress( req.body );
                    _.merge( customer_body, customer_coordinates );

                    [ customer, created ]  = await server.DB_MYSQL.models.customer.findOrCreate(
                        { where: { userId: customer_user.id, address: req.body.address }, defaults: customer_body }
                    );

                    req.body.customerId = customer.id;

                    return await trySave();
                }
                catch( ERR ){
                    debug( ERR );
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        /**
         * @swagger
         * path: /deliveries
         * operations:
         *   -  httpMethod: GET
         *      summary: Get deliveries
         *      notes: Return the deliveries
         *      responseClass: String
         *      nickname: get_deliveries
         *      consumes:
         *        - text/html
         */
        server
            .get( {
                name: 'get_deliveries',
                path: `${config.get( 'router.prefix' )}/deliveries`,
                version: '1.0.0',
            },
            server.UTILS.aclMiddleware( [ 'root', 'admin', 'client', 'driver' ] ),
            async ( req, res, next ) => {
                let deliveries;
                let conditions;

                conditions = {
                    where: {},
                    include: [
                        {
                            model: server.DB_MYSQL.models.client,
                            as: 'client',
                            include: [
                                {
                                    model: server.DB_MYSQL.models.user,
                                    as: 'user',
                                },
                            ],
                        },
                        {
                            model: server.DB_MYSQL.models.customer,
                            as: 'customer',
                            include: [
                                {
                                    model: server.DB_MYSQL.models.user,
                                    as: 'user',
                                },
                            ],
                        },
                    ],
                };

                if( !_.isEmpty( req.query ) ){
                    conditions.where.$or = [];
                    _.forIn( req.query, ( p, k ) => {
                        if( p.indexOf( ':' ) > -1 ){
                            switch( p.split( ':' )[ 0 ] ){
                            case( 'not' ):
                                let cond = {};
                                cond[ k ] = { $not: p.split( ':' )[ 1 ] };
                                conditions.where.$or.push( cond );
                                break;
                            }
                        }
                    } );
                }

                if( [ 'client', 'driver' ].indexOf( req.user.roleKey ) > -1 ){
                    conditions.where[ `${req.user.roleKey}Id` ] = req.user.dataValues[ req.user.roleKey ].id;
                }

                try{
                    deliveries = await server.DB_MYSQL.models.delivery.findAll( conditions );
                    return server.UTILS.jsonResponse( req, res, deliveries );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        /**
         * If the path matches deliveries/:id set the entity into the request object
         */
        server.use(
            async ( req, res, next ) => {
                switch( true ){
                case( new RegExp( 'deliveries/:id' ).test( req.route.path ) ):
                    let delivery;
                    try{
                        delivery =
                        await server.DB_MYSQL.models.delivery.findOne( {
                                where: {
                                    id: req.params.id,
                                },
                                include: [
                                    {
                                        model: server.DB_MYSQL.models.client,
                                        as: 'client',
                                    },
                                    {
                                        model: server.DB_MYSQL.models.customer,
                                        as: 'customer',
                                        include: [
                                            {
                                                model: server.DB_MYSQL.models.user,
                                                as: 'user',
                                            },
                                        ],
                                    },
                                    {
                                        model: server.DB_MYSQL.models.driver,
                                        as: 'driver',
                                        include: [
                                            {
                                                model: server.DB_MYSQL.models.user,
                                                as: 'user',
                                            },
                                        ],
                                    },
                                ],
                                raw: false,
                            } );
                        if( _.isNil( delivery ) || ( !req.user && delivery.status == 5 ) ) throw new Error( 'N_F' );
                        req.entity = delivery;
                        next();
                    }
                    catch( ERR ){
                        await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                        return server.UTILS.sendError( req, res, ERR );
                    }
                    break;
                default:
                    next();
                }
            } );

        /**
         * @swagger
         * path: /deliveries/{id}
         * operations:
         *   -  httpMethod: GET
         *      summary: Get the full delivery info
         *      notes: Return the delivery
         *      responseClass: String
         *      nickname: get_delivery
         *      consumes:
         *        - text/html
         *      parameters:
         *        - name: id
         *          in: path
         *          description: Delivery Key
         *          paramType: String
         *          required: true
         */
        server
            .get( {
                name: 'get_delivery',
                path: `${config.get( 'router.prefix' )}/deliveries/:id`,
                version: '1.0.0',
            },
            async ( req, res, next ) => {
                let entity = req.entity.get( { plain: true } );
                try{
                    // If no user logged in and no ?mobile=XXX token. Throw error.
                    if( !req.user && _.isNil( req.query.mobile ) ) throw new Error( 'Empty Token' );
                    // If no user logged in and ?mobile=XXX token is not correct. Throw error.
                    if( !req.user && !_.isNil( req.query.mobile ) &&
                        !ensurePhoneMatch( req.query.mobile, entity.customer.phone ) ) throw new Error( 'Invalid Token' );

                    if(
                        !req.user ||
                        (
                            req.user &&
                            (
                                [ 'client', 'driver' ].indexOf( req.user.roleKey ) > -1 &&
                                req.user.dataValues[ req.user.roleKey ].id != entity[ `${req.user.roleKey}Id` ]
                            )
                        )
                    ){
                        entity = _.pick( entity, [
                            'id',
                            'status',
                            'client_observation',
                            'customer_observation',
                            'updatedAt',
                            'createdAt',
                            'client.name',
                            'client.logo',
                            'client.address',
                            'client.phone',
                            'driver.user.name',
                            'driver.user.avatar',
                            'driver.lat',
                            'driver.lng',
                            'driver.updatedAt',
                            'customer.lat',
                            'customer.lng',
                            'customer.address',
                        ] );
                    }
                    
                    return server.UTILS.jsonResponse( req, res, _.omit( entity, [
                        'driver.user.password',
                        'driver.user.salt',
                        'driver.user.recovery_token',
                        'driver.user.provider_token',
                        'driver.user.provider_id',
                        'customer.user.password',
                        'customer.user.salt',
                        'customer.user.recovery_token',
                        'customer.user.provider_token',
                        'customer.user.provider_id',
                    ] ) );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );
            
        /**
        * @swagger
        * path: /deliveries/{id}
        * operations:
        *   -  httpMethod: PUT
        *      summary: Update an delivery
        *      notes: Return the updated delivery
        *      responseClass: String
        *      nickname: edit_delivery
        *      consumes:
        *        - text/html
        *      parameters:
        *        - name: id
        *          in: path
        *          description: Delivery id
        *          paramType: String
        *          required: true
        *        - name: name
        *          description: The delivery name
        *          paramType: form
        *          required: false
        *          dataType: String
        *        - name: email
        *          description: The delivery email
        *          paramType: form
        *          required: false
        *          dataType: String
        *        - name: password
        *          description: The delivery password
        *          paramType: form
        *          required: false
        *          dataType: String
        */
        server
            .put( {
                name: 'edit_delivery',
                path: `${config.get( 'router.prefix' )}/deliveries/:id`,
                version: '1.0.0',
            },
            server.UTILS.ownershipMiddleware( [ [ 'user.dataValues.client.id', 'entity.clientId' ], [ 'user.dataValues.driver.id', 'entity.driverId' ] ] ),
            async ( req, res, next ) => {
                let delivery;
                try{
                    // Allow the driver to only change the delivery status
                    if( req.user.roleKey == 'driver' ){
                        // It can only be changed to "on_the_way", "arrived" and "delivered"
                        if( parseInt( req.body.status ) > 1 && parseInt( req.body.status ) <= 5 ){
                            req.body = _.pick( req.body, [ 'status' ] );
                        }
                        else{
                            throw new Error( 'NOT_ALLWD' );
                        }
                    }

                    await Joi.validate( req.body, schema );
                    delivery = await req.entity.update( req.body );

                    if( _.isNil( delivery ) ) throw new Error( 'SV_RTRN_NIL' );
                    return server.UTILS.jsonResponse( req, res, _.merge( delivery, req.body ) );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        
        resolve( server );
    } );
};