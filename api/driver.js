const Joi = require( 'joi' );
const config = require( 'config' );
const debug = require( 'debug' )( 'API:NOWTRACK:DRIVER' );
const bcrypt = require( 'bcryptjs' );
const _   = require( 'lodash' );

const schema = Joi.object().keys( {
    user_id:        Joi.number().optional(),
    device_type:    Joi.string().min( 2 ).max( 255 ).optional(),
    device_version: Joi.string().min( 2 ).max( 255 ).optional(),
    device_battery: Joi.string().min( 2 ).max( 255 ).optional(),
    phone:          Joi.string().min( 2 ).max( 255 ).optional(),
    lat:            Joi.optional(),
    lng:            Joi.optional(),
    speed:          Joi.optional(),
} );

/**
 * @swagger
 * resourcePath: /drivers
 * description: Drivers endpoint
 */
module.exports = ( server ) => {
    return new Promise( ( resolve, reject ) => {
        /**
             * @swagger
             * path: /drivers
             * operations:
             *   -  httpMethod: POST
             *      summary: Create driver
             *      notes: Return the created driver
             *      responseClass: String
             *      nickname: new_driver
             *      consumes:
             *        - text/html
             *      parameters:
             
             */
        server
            .post( {
                name: 'new_driver',
                path: `${config.get( 'router.prefix' )}/drivers`,
                version: '1.0.0',
            },
            server.UTILS.aclMiddleware( [ 'root', 'admin', 'user' ] ),
            async ( req, res, next ) => {
                let driver;
                try{
                    await Joi.validate( req.body, schema );
                        
                    req.body.userId = req.user.id;

                    trySave = async () => {
                        driver = await server.DB_MYSQL.models.driver.create( req.body );
                        if( _.isNil( driver ) ) throw new Error( 'SV_RTRN_NIL' );
                        await req.user.update( { roleKey: 'driver' } );
                        return server.UTILS.jsonResponse( req, res, driver.get( { plain: true } ) );
                    };

                    return await trySave();
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        /**
             * @swagger
             * path: /drivers
             * operations:
             *   -  httpMethod: GET
             *      summary: Get drivers
             *      notes: Return the drivers
             *      responseClass: String
             *      nickname: get_drivers
             *      consumes:
             *        - text/html
             */
        server
            .get( {
                name: 'get_drivers',
                path: `${config.get( 'router.prefix' )}/drivers`,
                version: '1.0.0',
            },
            server.UTILS.aclMiddleware( [ 'root', 'admin', 'client', 'driver' ] ),
            async ( req, res, next ) => {
                let drivers;
                let conditions;
                
                conditions = {
                    where: {},
                    include: [
                        {
                            model: server.DB_MYSQL.models.user,
                            as: 'user',
                        },
                    ],
                };

                if( !_.isEmpty( req.query ) ){
                    _.forIn( req.query, ( v, k ) => {
                        conditions.where[ k ] = v;
                    } );
                }

                if( req.user.roleKey == 'driver' ){
                    conditions.where.userId = req.user.id;
                }

                if( [ 'root', 'admin' ].indexOf( req.user.roleKey ) < 0 ){
                    conditions.attributes = [
                        'id',
                        'phone',
                        'lat',
                        'lng',
                    ];
                    conditions.include[ 0 ].attributes = [ 'name', 'email', 'avatar' ];
                }

                try{
                    drivers = await server.DB_MYSQL.models.driver.findAll( conditions );
                    return server.UTILS.jsonResponse( req, res, drivers );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        /**
             * If the path matches drivers/:id set the entity into the request object
             */
        server.use(
            async ( req, res, next ) => {
                switch( true ){
                case( new RegExp( 'drivers/:id' ).test( req.route.path ) ):
                    let driver;

                    let conditions = {
                        where: {
                            id: req.params.id,
                        },
                        raw: false,
                    };

                    if( req.method == 'GET' ){
                        conditions.include = [
                            {
                                model: server.DB_MYSQL.models.user,
                                as: 'user',
                            },
                            {
                                model: server.DB_MYSQL.models.delivery,
                                as: 'deliveries',
                            },
                        ];
                    }
                    try{
                        driver = await server.DB_MYSQL.models.driver.findOne( conditions );
                        if( _.isNil( driver ) ) throw new Error( 'N_F' );
                        req.entity = driver;
                        next();
                    }
                    catch( ERR ){
                        await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                        return server.UTILS.sendError( req, res, ERR );
                    }
                    break;
                default:
                    next();
                }
            }
        );
            
        /**
             * @swagger
             * path: /drivers/{id}
             * operations:
             *   -  httpMethod: GET
             *      summary: Get an driver
             *      notes: Return the driver
             *      responseClass: String
             *      nickname: get_driver
             *      consumes:
             *        - text/html
             *      parameters:
             *        - name: id
             *          in: path
             *          description: Driver ID
             *          paramType: String
             *          required: true
             */
        server
            .get( {
                name: 'get_driver',
                path: `${config.get( 'router.prefix' )}/drivers/:id`,
                version: '1.0.0',
            },
            server.UTILS.ownershipMiddleware( 'user.id', 'entity.userId' ),
            async ( req, res, next ) => {
                try{
                    return server.UTILS.jsonResponse( req, res, req.entity );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );
            
        /**
            * @swagger
            * path: /drivers/{id}
            * operations:
            *   -  httpMethod: PUT
            *      summary: Update an driver
            *      notes: Return the updated driver
            *      responseClass: String
            *      nickname: edit_driver
            *      consumes:
            *        - text/html
            *      parameters:
            *        - name: id
            *          in: path
            *          description: Driver ID
            *          paramType: String
            *          required: true
            *        - name: name
            *          description: The driver name
            *          paramType: form
            *          required: false
            *          dataType: String
            *        - name: email
            *          description: The driver email
            *          paramType: form
            *          required: false
            *          dataType: String
            *        - name: password
            *          description: The driver password
            *          paramType: form
            *          required: false
            *          dataType: String
            */
        server
            .put( {
                name: 'edit_driver',
                path: `${config.get( 'router.prefix' )}/drivers/:id`,
                version: '1.0.0',
            },
            server.UTILS.ownershipMiddleware( 'user.id', 'entity.userId' ),
            async ( req, res, next ) => {
                let driver;
                try{
                    await Joi.validate( req.body, schema );
                    if( !_.isNil( req.body.password ) ){
                        req.body.salt = bcrypt.genSaltSync( 8 );
                        req.body.password = bcrypt.hashSync( req.body.password, req.body.salt );
                    }
                    driver = await req.entity.update( req.body );
                    if( _.isNil( driver ) ) throw new Error( 'SV_RTRN_NIL' );
                    return server.UTILS.jsonResponse( req, res, _.merge( driver.get( { plain: true } ), req.body ) );
                }
                catch( ERR ){
                    console.log( ERR );
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );
            
        /**
             * @swagger
             * path: /drivers/{id}
             * operations:
             *   -  httpMethod: DELETE
             *      summary: Delete an driver by ID
             *      notes: Deletes an driver from a given ID
             *      responseClass: String
             *      nickname: delete_driver
             *      consumes:
             *        - text/html
             *      parameters:
             *        - name: id
             *          in: path
             *          description: Driver ID
             *          paramType: String
             *          required: true
            */
        server
            .del( {
                name: 'delete_driver',
                path: `${config.get( 'router.prefix' )}/drivers/:id`,
                version: '1.0.0',
            },
            server.UTILS.ownershipMiddleware( 'user.id', 'entity.userId' ),
            async ( req, res, next ) => {
                try{
                    await req.entity.destroy();
                    return server.UTILS.jsonResponse( req, res );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );
        
        resolve( server );
    } );
};