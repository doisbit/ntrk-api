const config = require( 'config' );
const _ = require( 'lodash' );
const debug = require( 'debug' )( 'API:NOWTRACK:LOCATION' );

/**
 * @swagger
 * resourcePath: /location
 * description: Location endpoint
 */
module.exports = ( server ) => {
    return new Promise( ( resolve, reject ) => {
        /**
             * @swagger
             * path: /location
             * operations:
             *   -  httpMethod: POST
             *      summary: Update the driver location sent
             *      responseClass: String
             *      nickname: new_location_update
             *      consumes:
             *        - text/html
             *      parameters:
             
             */
        server
            .post( {
                name: 'new_location_update',
                path: `${config.get( 'router.prefix' )}/location`,
                version: '1.0.0',
            },
            server.UTILS.aclMiddleware( [ 'root', 'admin', 'driver' ] ),
            async ( req, res, next ) => {
                let location;
                let driver;
                debug( req.body );
                try{
                    location = req.body[ Object.keys( req.body ).pop() ];
                    driver = await server.DB_MYSQL.models.driver.findOne( { where: { userId: req.user.id } } );
                    if( _.isNil( driver ) ) throw new Error( 'SV_RTRN_NIL' );
                    await driver.update( { lat: location.latitude, lng: location.longitude } );
                    return server.UTILS.jsonResponse( req, res );
                }
                catch( ERR ){
                    await server.DB_MYSQL.models.log.create( server.UTILS.generateLogStack( ERR, req ) );
                    return server.UTILS.sendError( req, res, ERR );
                }
            } );

        resolve( server );
    } );
};