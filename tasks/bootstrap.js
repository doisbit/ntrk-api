module.exports = ( db ) => {
    return new Promise( async ( resolve, reject ) => {
        try{
            await db.models.role.findOrCreate( { where: { key: 'client' }, defaults: { hierarchy: 4 } } );
            await db.models.role.findOrCreate( { where: { key: 'driver' }, defaults: { hierarchy: 5 } } );
            resolve();
        }
        catch( ERR ){
            reject( ERR );
        }
    } );
};