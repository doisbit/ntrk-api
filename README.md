## Nomenclatures

#### Driver
The guy who drives the deliveries
#### Customer
The person who bought something and now is waiting to receive his delivery.

#### Client
The entity (fastfood, library, office in general) that hires our services in order to create deliveries to his customers.

#### Delivery
The entity

## Delivery statuses

#### 1
Created. The client has created a new delivery and assigned a driver.

#### 2
Assigned